/**
 * Support Functions
 */
function trim(value) {
	return BirtStr.trim(value);
}

/**
 * Prototype Functions
 */
String.prototype.trim = function() {
	return BirtStr.trim(this);
}

String.prototype.reverse = function() {
	splitext = this.split("");
	revertext = splitext.reverse();
	reversed = revertext.join("");
	return reversed;
}

String.prototype.repeat = function(count) {
	var output = "";
	for(var i = 0; i < count; i++) {
		output += this;
	}
	return output;
}

/**
 * New API to use on Eclipse birt reports
 */
CMSReport = {
	maskCard : function(card, apply) {
		if(card == null)
			return "";
	
		var res = card.toString();
		
		if (apply === undefined || apply == "S") {
			card = card.toString().trim();
			var n = card.length;
			if (n > 9) {
				res = card.substr(0, 6);
				res += '*'.repeat(n - 10);
				res += card.substr(n - 4);
			}
		}
		return res;
	},
	
	maskCardPercent : function(card, apply, maskedPercentage) {
		if(card == null)
			return "";
		
		var res = card.toString().trim();
		var cardLen = card.toString().trim().length;		
		var maskedCharacters = 0;
		var finalCharacters = 4;		
		if(maskedPercentage > 0 && maskedPercentage <= 100 ) {
			maskedCharacters = Math.round((cardLen - 6) * (maskedPercentage / 100));
			finalCharacters = (cardLen - 6) - maskedCharacters;
		}
				
		if (apply === undefined || apply == "S") {
			card = card.toString().trim();			
			if (cardLen > 9) {
				res = card.substr(0, 6);
				res += '*'.repeat(cardLen - (6 + finalCharacters));
				res += card.substr(cardLen  - finalCharacters);
			}
		}
		return res;
	},

	formatDate : function(dateStr) {
		dateStr = dateStr.toString().trim();
		if (dateStr == "" || dateStr == "0" || dateStr.length != 8) {
			return dateStr;
		} else {
			var year = dateStr.substring(0, 4);
			var month = dateStr.substring(4, 6);
			var day = dateStr.substring(6, 8);

			if (reportContext.getLocale().match(/en(_\w*)?/g)) {
				return month + "/" + day + "/" + year;
			} else {
				return day + "/" + month + "/" + year;
			}
		}
	},
	
	formatDateYYYYMMDD : function(dateStr) {
		dateStr = dateStr.toString().trim();
		if (dateStr == "" || dateStr == "0" || dateStr.length != 8) {
			return dateStr;
		} else {
			var year = dateStr.substring(0, 4);
			var month = dateStr.substring(4, 6);
			var day = dateStr.substring(6, 8);

			return year + "-" + month + "-" + day;
		}
	},

	formatPeriod : function(dateStr) {
		dateStr = dateStr.toString().trim();
		if (dateStr == "" || dateStr == "0" || dateStr.length != 6) {
			return dateStr;
		} else {
			var year = dateStr.substring(0, 4);
			var month = dateStr.substring(4, 6);
			return month + "/" + year;
		}
	},

	formatShortPeriod : function(dateStr) {
		dateStr = dateStr.toString().trim();
		if (dateStr == "" || dateStr == "0" || dateStr.length != 4) {
			return dateStr;
		} else {
			var year = dateStr.substring(0, 2);
			var month = dateStr.substring(2, 4);
			return month + "/" + year;
		}
	},

	formatTime : function(strTime) {
		if(!isDefined(strTime) || strTime == null){
			return "";
		}
		strTime = strTime.toString().trim();
		if (strTime == "") {
			return strTime;
		} 
		strTime = "000000" + strTime;
		strTime = strTime.substring(strTime.length - 6);
		return strTime.substring(0, 2) + ":" + strTime.substring(2, 4) + ":" + strTime.substring(4);
	},
	
	isDefined : function(param){
		return typeof param != 'undefined';
	}
}

// -----------------------------------------------------------------------------------------------------------------------------

var issuerJNDI = "java:/ISSUER_DS";
var acquirerJNDI = "java:/ACQUIRER_DS";
var iKey_a = ("a").charCodeAt(0);
var iKey_z = ("z").charCodeAt(0);
var iKey_A = ("A").charCodeAt(0);
var iKey_Z = ("Z").charCodeAt(0);
var iKey_0 = ("0").charCodeAt(0);
var iKey_9 = ("9").charCodeAt(0);
var iKey_comma = (",").charCodeAt(0);
var iKey_period = (".").charCodeAt(0);
var iKey_space = (" ").charCodeAt(0);

var months = new Array(13);
months[0] = "";
months[1] = "Jan";
months[2] = "Feb";
months[3] = "Mar";
months[4] = "Apr";
months[5] = "May";
months[6] = "Jun";
months[7] = "Jul";
months[8] = "Aug";
months[9] = "Sep";
months[10] = "Oct";
months[11] = "Nov";
months[12] = "Dec";

function getJndi(submodel) {
	if (submodel == "A") {
		return acquirerJNDI;
	} else if (submodel == "E") {
		return issuerJNDI;
	} else {
		return "";
	}
}

function formatDate(dateToFormat) {
	if (dateToFormat == null || dateToFormat == "")
		return "";

	var strDate = "";
	var longDate = dateToFormat;

	if (dateToFormat == "0") {
		strDate = "00/00/0000";
	} else {
		var yyyy = longDate.toString().substring(0, 4);
		var mm = longDate.toString().substring(4, 6);
		var dd = longDate.toString().substring(6, 8);

		if (reportContext.getLocale() == "en_EN"
				|| reportContext.getLocale() == "en_US"
				|| reportContext.getLocale() == "en") {
			strDate = mm + "/" + dd + "/" + yyyy;
		} else {
			strDate = dd + "/" + mm + "/" + yyyy;
		}
	}

	return strDate;
}

function formatShortDate(dateToFormat) {

	if (dateToFormat == null || dateToFormat == "")
		return "";

	var strDate = "";
	var longDate = dateToFormat;

	if (dateToFormat == "0") {
		strDate = "";
	} else {
		var yyyy = longDate.toString().substring(2, 4);
		var mm = longDate.toString().substring(4, 6);
		if (mm < 10) {
			var mm = mm.toString().substring(1, 2);
		}
		var monthname = months[mm];
		strDate = monthname + '-' + yyyy;
	}
	return strDate;
}

function getResource(key) {
	return reportContext.getMessage(key, reportContext.getLocale());
}

function documentMask(strDoc, strMask) {
	var strDocAux = "";
	var strCaracter = "";
	var intContadorDoc = 0;

	if (strDoc == "" || strMask == "" || strMask == null) {
		return strDoc;
	}

	for ( var i = 0; i < strMask.length; i++) {
		strCaracter = strMask.substring(i, i + 1);

		if (strCaracter == "X" || strCaracter == "x" || strCaracter == "#") {
			strDocAux += strDoc.substring(intContadorDoc, intContadorDoc + 1);
			intContadorDoc += 1;
		} else {
			strDocAux += strMask.substring(i, i + 1);
		}

	}

	return strDocAux;
}

function getParamDescription(param, description) {
	var paramDescription = "";
	if (param != null) {
		if (description != null && description != "")
			paramDescription = param + " - " + description;
		else
			paramDescription = param;
	} else {
		paramDescription = getResource("text.all");
	}
	return paramDescription.toString();
}

function longToTime(lngTime) {
	var sHours = "";
	var sMinutes = "";
	var sSeconds = "";

	var sTime = lngTime;

	sTime = "000000" + sTime;
	sTime = sTime.substring(sTime.length - 6);

	sHours = sTime.substring(0, 2);
	sMinutes = sTime.substring(2, 4);
	sSeconds = sTime.substring(4, 6);

	sTime = sHours + ":" + sMinutes + ":" + sSeconds;

	return sTime;
}

function setMask(miMask, strValue) {
	var blnSalir = false;
	var strReturn = "";
	var strCharAux = "";
	var strMask = miMask;

	var iLengthMask = strMask.length;
	var iContMask = 0;
	var iLengthValue = strValue.length;
	var iContValue = 0;

	while (!blnSalir) {
		strChar = strValue.substr(iContValue, 1);
		strCharMask = strMask.substr(iContMask, 1);

		iKeyCode = strChar.charCodeAt(0);

		if (strCharMask == "#") {
			if ((((iKeyCode >= iKey_a) && (iKeyCode <= iKey_z)) || ((iKeyCode >= iKey_A) && (iKeyCode <= iKey_Z)))) {
				// alert("<bean:message key='validate.valueMustBeNumeric'/>");
				return false;

			}
			strCharAux = strChar;

			iContMask++;
			iContValue++;

		} else if (strCharMask == "?") {
			if (!(((iKeyCode >= iKey_a) && (iKeyCode <= iKey_z)) || ((iKeyCode >= iKey_A) && (iKeyCode <= iKey_Z)))) {
				// alert("<bean:message
				// key='validate.valueMustBeAlphabetic'/>");
				return false;
			}
			strCharAux = strChar;

			iContMask++;
			iContValue++;

		} else {
			strCharAux = strCharMask;
			iContMask++;
		}
		strReturn += strCharAux;

		if ((iContValue == iLengthValue) || (iContMask == iLengthMask)) {
			blnSalir = true;
		}
	}
	return strReturn;
}

function formatNumber(dblNumber, intDecimals) {
	var arrNumberParts = (String(dblNumber)).split(".")

	var decFill = "";
	for (i = 1; i <= intDecimals; i++)
		decFill += "0";

	var strDecimals = decFill;
	if (arrNumberParts.length == 2) {
		strDecimals = arrNumberParts[1] + strDecimals;
	}

	return arrNumberParts[0] + "." + strDecimals.substr(0, intDecimals);
}

function formatNumberEspPorEn(value, strMask) {
	var strThousandSeparator = "";
	var strDecimalSeparator = "";
	var hasMinusSign = false;
	var ret = "";

	if (reportContext.getLocale() == "en_EN"
			|| reportContext.getLocale() == "en_US"
			|| reportContext.getLocale() == "en") {
		strThousandSeparator = ",";
		strDecimalSeparator = ".";
	} else {
		strThousandSeparator = ".";
		strDecimalSeparator = ",";
	}

	var strValue = value.toString();

	if (strValue != "") {

		if (strValue.indexOf("-") != -1) {
			hasMinusSign = true;
			strValue = strValue.substr(1, strValue.length);
		}

		var numbers = (String(strValue)).split(".")
		var intportion = numbers[0];
		var decimalportion = numbers[1];

		var strMaskParts = strMask.split(".");
		var strDecimalDigits = strMaskParts[1];
		if (decimalportion == undefined) {
			decimalportion = "";
			var k = 0;

			for (k; k < strDecimalDigits.length; k++) {
				decimalportion += "0";
			}
		} else {
			while (decimalportion.length < strDecimalDigits.length) {
				decimalportion += "0";
			}
		}

		var j = intportion.length - 1;
		var auxValue = "";
		var i = 0;

		for (j; j >= 0; j--) {
			var strDigit = intportion.substr(j, 1);
			i++;
			auxValue = strDigit + auxValue;

			if (i == 3 && j > 0) {
				i = 0;
				auxValue = strThousandSeparator + auxValue;
			}
		}

		ret = auxValue + strDecimalSeparator + decimalportion;

		if (hasMinusSign) {
			ret = "-" + ret;
		}

		return ret;
	} else {
		return 0 + strDecimalSeparator + "00";
	}
}

function toFixed(value, cantDecimales) {
	return value.toFixed(cantDecimales);
}

function formatTime (strTime) {
	if(!isDefined(strTime) || strTime == null){
		return "";
	}
	strTime = strTime.toString().trim();
	if (strTime == "") {
		return strTime;
	} 
	strTime = "000000" + strTime;
	strTime = strTime.substring(strTime.length - 6);
	return strTime.substring(0, 2) + ":" + strTime.substring(2, 4) + ":" + strTime.substring(4);
}

function isDefined (param){
	return typeof param != 'undefined';
}

function longToPeriod(nPeriod) {

	if (nPeriod == "" || nPeriod == null)
		return "";

	var strDate = "";
	var longDate = nPeriod;

	if (nPeriod == "0" || nPeriod == 0) {
		strDate = "00/0000";
	} else {
		var yyyy = longDate.toString().substring(0, 4);
		var mm = longDate.toString().substring(4, 6);

		strDate = mm + "/" + yyyy;

	}

	return strDate;
}

function leftPadWithCeros(plngLengthCeros, plngValue) {
	var aux = "";

	if (plngLengthCeros > plngValue.length) {
		var numberOfCerosToAdd = plngLengthCeros - plngValue.length;

		for ( var i = 0; i < numberOfCerosToAdd; i++) {
			aux = "0" + aux;
		}
	}
	return (aux + plngValue);
}

function previousPeriod(strDate) {
	var strPreviousPeriod;
	var partes = strDate.split("/");
	var parte0 = partes[0];
	var parte1 = partes[1];
	if (partes[0] == "1" || partes[0] == "01") {
		parte0 = "12";
		year = parseFloat(parte1) - 1;
		parte1 = year.toString();
	} else {
		month = parseFloat(parte0) - 1;
		parte0 = month.toString();
	}
	strPreviousPeriod = parte0 + "/" + parte1;
	return strPreviousPeriod;
}

function ShortDateAddPeriods(longDate, longPeriods) {

	if (longDate == "" || longDate == null)
		return "";

	if (longPeriods == "" || longPeriods == null)
		return longDate;

	var yyyy = longDate.toString().substring(0, 4);
	var mm = longDate.toString().substring(4, 6);
	var dd = longDate.toString().substring(6, 8);
	var strAddPeriods = "";
	for ( var i = 0; i < longPeriods; i++) {
		if (mm == "12") {
			mm = "01";
			yyyy = parseFloat(yyyy) + 1;
		} else {
			mm = parseFloat(mm) + 1;
		}
	}
	var monthname = months[mm];
	strAddPeriods = monthname + '-' + yyyy;
	return strAddPeriods;

}

function ShortDateSubPeriods(longDate, longPeriods) {

	if (longDate == "" || longDate == null)
		return "";
	if (longPeriods == "" || longPeriods == null)
		return longDate;
	var strDate = "";
	var yyyy = longDate.toString().substring(0, 4);
	var mm = longDate.toString().substring(4, 6);
	var dd = longDate.toString().substring(6, 8);
	var strSubPeriods = "";
	for ( var i = 0; i < longPeriods; i++) {
		if (mm == "1" || mm == "01") {
			mm = "12";
			yyyy = parseFloat(yyyy) - 1;
		} else {
			mm = parseFloat(mm) - 1;
		}
	}
	var monthname = months[mm];
	strSubPeriods = monthname + '-' + yyyy;
	return strSubPeriods;

}

function labelSubtractionPeriod(period, count) {
	if (count < 0) {
		return "";
	}
	if (count == 0) {
		var partes = period.split("/");
		var parte0 = partes[0];
		var parte1 = partes[1];
		return parte0;
	} else {
		return labelSubtractionPeriod(previousPeriod(period), count - 1);
	}
}

function formatCero(str, countCero) {
	var result;

	if (str == null || str == "") {
		result = "";
	} else {
		result = str.toString();
	}

	var count = countCero - result.length;

	for ( var i = 0; i < count; i++) {
		result = "0" + result;
	}

	return result;
}

// Diferrencia de dias entre dos fechas con formato "YYYYMMDD"
// (lngDate = dateFrom) - (lngDate2 = dateTo)
function diffLngDateDays(lngDate, lngDate2) {
	if ((lngDate == null) || (lngDate2 == null) || (lngDate > lngDate2)) {
		return -1
	} else {
		lngDate = lngDate.toString();
		var ano1 = lngDate.substring(0, 4);
		var mes1 = lngDate.substring(4, 6);
		var dia1 = lngDate.substring(6, 8);
		var ano2 = lngDate2.substring(0, 4);
		var mes2 = lngDate2.substring(4, 6);
		var dia2 = lngDate2.substring(6, 8);

		return diffDays(dia1, mes1, ano1, dia2, mes2, ano2);
	}
}

function diffDays(dia1, mes1, ano1, dia2, mes2, ano2) {
	var fecha1 = new Date(ano1, mes1 - 1, dia1);
	var fecha2 = new Date(ano2, mes2 - 1, dia2);
	var resta = (fecha2 - fecha1) / 1000 / 3600 / 24;

	return resta;
}

// Retorna todos los elementos que matchean con la expReg
RegExp.prototype.execAll = function(string) {
	var match = null;
	var matches = new Array();
	while (match = this.exec(string)) {
		matches.push(new Array(match[1], match[0]));
	}
	return matches;
}

function getFromJSONParameter(paramName, params) {
	// saco el arroba del comienzo.
	var paramName = paramName.replace("@", "");
	var values = eval(params[paramName].value);
	var valueSet = "(";
	var largo = 0;
	if (values != null) {
		largo = values.length
	}

	for (j in values) {
		if (parseInt(j) == j) {
			valueSet = valueSet + "'" + values[j] + "'";
			if (largo - 1 > j) {
				valueSet = valueSet + ",";
			}
		}
	}
	valueSet = valueSet + ")";
	return valueSet;

}

function completeListParams(params) {
	var queryInPatter = "@[^@]*(@[A-z]*)(@)";
	var regExpN = new RegExp(queryInPatter, "g")
	var matches = regExpN.execAll(this.queryText);
	for (i in matches) {
		if (parseInt(i) == i) {
			// obtener la lista para el parametro
			var arrParam = trim(matches[i][0]);
			var line = trim(matches[i][1]);
			var strVal = getFromJSONParameter(arrParam, params);
			var lineAnt = line;
			line = line.replace(arrParam, strVal.toString());
			line = deleteArrobas(line);

			// remplazar el string por la lista
			if (strVal != "()") {
				this.queryText = this.queryText.replace(lineAnt, line);
			} else {
				this.queryText = this.queryText.replace(lineAnt, "");
			}
		}
	}
}


function isSameMonth(transactionDate, sysDate) {
	var result = 0;
	transactionDate = transactionDate.toString();
	sysDate = sysDate.toString();
	var ano1 = transactionDate.substring(0, 4);
	var mes1 = transactionDate.substring(4, 6) - 1;
	var dia1 = transactionDate.substring(6, 8);
	var ano2 = sysDate.substring(0, 4);
	var mes2 = sysDate.substring(4, 6) - 1;
	var dia2 = sysDate.substring(6, 8);

	txnDate = new Date(ano1, mes1, dia1);
	today = new Date(ano2, mes2, dia2);

	var monthActual = today.getMonth();
	var yearActual = today.getFullYear();
	
	if (monthActual == txnDate.getMonth() && yearActual == txnDate.getFullYear()) {
		result = 1;
	}

	return result;
}

function isSameFortnight(transactionDate, sysDate){
	var result = 0;
	transactionDate = transactionDate.toString();
	sysDate = sysDate.toString();
	var ano1 = transactionDate.substring(0, 4);
	var mes1 = transactionDate.substring(4, 6) - 1;
	var dia1 = transactionDate.substring(6, 8);
	var ano2 = sysDate.substring(0, 4);
	var mes2 = sysDate.substring(4, 6) - 1;
	var dia2 = sysDate.substring(6, 8);
	
	txnDate = new Date(ano1, mes1, dia1);
	today = new Date(ano2, mes2, dia2);
	
	var dayActual= today.getDate();
    var monthActual = today.getMonth();
    var yearActual = today.getFullYear();
    
	if(monthActual == txnDate.getMonth() && yearActual == txnDate.getFullYear()){
		if(dia1 < 15 && dia2 < 15){
			result = 1;
		}else if(dia1 > 15 && dia2 > 15){
			result = 1;
		}
	}	
	
    return result;
}


function isSameWeek(transactionDate, sysDate) {
	var result = 0;
	transactionDate = transactionDate.toString();
	sysDate = sysDate.toString();
	var ano1 = transactionDate.substring(0, 4);
	var mes1 = transactionDate.substring(4, 6) - 1;
	var dia1 = transactionDate.substring(6, 8);
	var ano2 = sysDate.substring(0, 4);
	var mes2 = sysDate.substring(4, 6) - 1;
	var dia2 = sysDate.substring(6, 8);

	txnDate = new Date(ano1, mes1, dia1);
	today = new Date(ano2, mes2, dia2);

	var dateActual = today.getDate();
	var dayActual = today.getDay();
	var monthActual = today.getMonth();
	var yearActual = today.getFullYear();
	var diffDays = Math.abs(diffLngDateDays(sysDate - transactionDate));

	var firstdayMonthActual = new Date(ano2, mes2, 1);
	var firstTxnDate = new Date(ano1, mes1, 1);

	if (monthActual == txnDate.getMonth()
			&& yearActual == txnDate.getFullYear()) {
		if (diffDays < 7) {
			if (dayActual > txnDate.getDay()
					&& dayActual - diffDays >= 0) {
				result = 1;
			} else if (dayActual < txnDate.getDay()
					&& dayActual + diffDays <= 6) {
				result = 1;
			}
		}
	}
	return txnDate.getDay();
}


function deleteArrobas(text) {
	return text.replace(/@/g, "");
}

function maskCardExpiration(text) {
	return text.replace(/\d/g, "x");
}

function getResourceTask(taskId, module) {
	var task = taskId.toString().replace(/[^\d+]/g, '');
	if(module == "A") {
		return reportContext.getMessage("acqr.task." + task, reportContext.getLocale());
	} else if( module == "E") {
		return reportContext.getMessage("issr.task." + task, reportContext.getLocale());
	}
	return "No se encontro la key";
}


function convertJsonToNumberValueList(json) {
	//remove [] to json, take values in String
	var valueConvert = json.toString().replace(/[\[\]]+/g,'');
	// remove String and convert number
	valueConvert = valueConvert.replace(/['"]+/g,''); 	
	return valueConvert;
}
